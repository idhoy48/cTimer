/*
 * A library to implement a timer
 * Created by Ben Tenney, March 16, 2018.
 * 
 * 
 * 
 * */



#ifndef cTimer_h
#define cTimer_h

#if (ARDUINO >= 100)
  #include <Arduino.h>
#endif
#include <string.h>



class cTimer
{
  public:
    ///construtors
    cTimer();
    cTimer(unsigned long ms, unsigned long seconds=0, unsigned long minutes=0, unsigned long hours=0);
    ///setup functions
    void setTimer(unsigned long ms=0, unsigned long seconds=0, unsigned long minutes=0, unsigned long hours=0);
    void setSeconds(unsigned int seconds);
    void setMinutes(unsigned int minutes);
    void setHours(unsigned int hours);
    ///control functions
    int start();                                                        //return values: 0 if stopped, 1 if running, -1 if timer has runout
    int stop();                                                         //return values: 0 if stopped, 1 if running, -1 if timer has runout
    int reset();                                                        //return values: 0 if stopped, 1 if running, -1 if timer has runout
    //int restart();                                                      //return values: 0 if stopped, 1 if running, -1 if timer has runout
    int updateTimer();                                                  //return values: 0 if stopped, 1 if running, -1 if timer has runout
    ///print functions
    //String printTime();
    char* printTime(char * t);
    //String printTimeElapsed(unsigned int format=0);
    char* printSQLElapsed(char* t);                                     //print mySQL compatible time format
    unsigned long getHours(int total=0);                                //total=0 (default) show hours minus days; total=1 show total hours
    unsigned long getMinutes(int total=0);                              //total=0 (default) show minutes minus hours; total=1 show total minutes
    unsigned long getSeconds(int total=0);                              //total=0 (default) show seconds minus minutes; total=1 show total seconds
    unsigned long getMSElapsed();                                       //returns elapsed time in milliseconds
    
  private:
    ///variables
    const unsigned long _MULTIPLIER = 1000;                             //multiplier to convert seconds to milliseconds
    boolean _cDown;                                                     //set to true to count down from a value, false to count up from a value
    boolean _stopped;                                                   //is the clock stopped
    boolean _done;                                                      //has the cock finished
    unsigned long _rClock;                                              //running clock in milliseconds
    unsigned long _sClock;                                              //static clock value in milliseconds
    unsigned long _sMillis;                                             //millisecond count at start
};



#endif
