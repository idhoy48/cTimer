/*
 * 
 * 
 * 
 * */


 
#include "cTimer.h"



////////////////////////////////////////////////////////////////////////
//Constructors
////////////////////////////////////////////////////////////////////////

cTimer::cTimer()
{
  setTimer(0);
}

cTimer::cTimer(unsigned long ms, unsigned long seconds, unsigned long minutes, unsigned long hours)
{
  setTimer(ms, seconds, minutes, hours);
}

////////////////////////////////////////////////////////////////////////
//Setup functions
////////////////////////////////////////////////////////////////////////

void cTimer::setTimer(unsigned long ms, unsigned long seconds, unsigned long minutes, unsigned long hours)
{
  _stopped = true;                                                      //clock is stopped by default
  _done = false;                                                        //clock is set to not done
  //Convert the time to ms
  ms += (hours*3600*_MULTIPLIER) + (minutes*60*_MULTIPLIER) + (seconds*_MULTIPLIER);
  //if seconds is greater than 0 count down is set to true else it is set to a count up timer
  (ms > 0) ? _cDown = true : _cDown = false;
  //set the rClock to 0 and sClock to the time in milliseconds
  _rClock = 0;
  _sClock = ms;
}

void cTimer::setSeconds(unsigned int seconds)
{
  setTimer(seconds * _MULTIPLIER);
}

void cTimer::setMinutes(unsigned int minutes)
{
  setTimer(minutes * 60 * _MULTIPLIER);
}

void cTimer::setHours(unsigned int hours)
{
  setTimer(hours * 3600 * _MULTIPLIER);
}

////////////////////////////////////////////////////////////////////////
//Control functions
////////////////////////////////////////////////////////////////////////

int cTimer::start()
{
  if(_done)
  {
    return -1;                                                          //if timer has finished return value of -1
  }
  else
  {
    if(_stopped)                                                        //if timer is stopped start it and return 1
    {
      _sMillis = millis();
      _stopped = false;
      return 1;
    }
  }
  return updateTimer();                                                 //if the timer is running update it
}

int cTimer::stop()
{
  int status = updateTimer();                                           //update the timer and get the status so that it can be started again from where it left off
  if(status == 1)
  {
    _stopped = true;                                                    //Stop the timer
    return 0;                                                           //return the stopped status
  }
  else return status;                                                   //if the timer is already stopped or has finished return that status
}

int cTimer::reset()
{
  _done = false;                                                        //reset the done flag
  _stopped = true;                                                      //reset the stopped flag
  _rClock = 0;                                                          //reset running clock to start time
  return 0;                                                             //return stopped value
}

int cTimer::updateTimer()
{
  if(_done)                                                             //if timer has finished return completed value
  {
    return -1;
  }
  else
  {
    if(_stopped)                                                        //if timer is stopped return stopped value
    {
      return 0;
    }
    else
    {
      unsigned long currentMillis = millis();                           //get the current millisecond count
      _rClock += currentMillis - _sMillis;                              //update the running clock
      _sMillis = currentMillis;                                         //update the millisecond count
      if(_sClock != 0)                                                  //if running a count down timer, check if the countdown is completed
      {
        if(_rClock >= _sClock)                                          //if the running clock is past the time
        {
          _stopped = true;                                              //stop the clock
          _done = true;                                                 //mark it as finished
          _rClock = _sClock;                                            //and set the running clock to the run time to prevent overrun
          return -1;
        }
      }
      return 1;                                                         //clock must still be running, return running value
    }
  }
  return -2;                                                            //if we get this far something is wrong!
}

////////////////////////////////////////////////////////////////////////
//Print functions
////////////////////////////////////////////////////////////////////////

/*String cTimer::printTime()
{
  updateTimer();
  String val = "";
  int hours = getHours();
  int min = getMinutes();
  int sec = getSeconds();
  if(hours)
  {
    val += String(hours) + ":" + (min<10?"0":"");
  }
  val += String(min) + ":" + (sec<10?"0":"");
  val += String(sec);
  return val;
}*/

char* cTimer::printTime(char * t)
{
  updateTimer();
  if(t == NULL)
    return NULL;
  t[0] = '\0';
  char temp[3];
  int hours = getHours();
  int min = getMinutes();
  int sec = getSeconds();
  if(hours)
  {
    itoa(hours, temp, 10);
    strcat(t, temp);
    strcat(t, ":");
    if(min < 10)
      strcat(t, "0");
  }
  itoa(min, temp, 10);
  strcat(t, temp);
  strcat(t, ":");
  if(sec < 10)
    strcat(t, "0");
  itoa(sec, temp, 10);
  strcat(t, temp);
  return t;
}

/*String cTimer::printTimeElapsed(unsigned int format)
{
  //if format == 0 print in a standard clock format
  //if format == 1 print in a mysql compatible format
  updateTimer();
  String val = "";  //rclock
  int hours = (_rClock / (3600 * _MULTIPLIER)) % 24;
  int min = (_rClock / (60 * _MULTIPLIER)) % 60;
  int sec = (_rClock / _MULTIPLIER) % 60;
  unsigned int milli = _rClock % 1000;
  if(hours)
  {
    val += String(hours) + (format==1?"":":") + (min<10?"0":"");
  }
  val += String(min) + (format==1?"":":") + (sec<10?"0":"");
  val += String(sec);
  val += (format==1?"."+String(milli):"");
  return val;
}*/

char* cTimer::printSQLElapsed(char* t)
{
  updateTimer();
  if(t == NULL)
    return NULL;
  t[0] = '\0';
  char temp[3];
  int hours = (_rClock / (3600 * _MULTIPLIER)) % 24;
  int min = (_rClock / (60 * _MULTIPLIER)) % 60;
  int sec = (_rClock / _MULTIPLIER) % 60;
  unsigned int milli = _rClock % 1000;
  if(hours)
  {
    itoa(hours, temp, 10);
    strcat(t, temp);
    if(min < 10)
      strcat(t, "0");
  }
  itoa(min, temp, 10);
  strcat(t, temp);
  if(sec < 10)
    strcat(t, "0");
  itoa(sec, temp, 10);
  strcat(t, temp);
  strcat(t, ".");
  for(int i=100; i>1; i /= 10)
    if(milli < i)
      strcat(t, "0");
  itoa(milli, temp, 10);
  strcat(t, temp);
  return t;
}

unsigned long cTimer::getHours(int total)
{
  if(total == 0)                                                        //if total = 0 (false) return hours modulo 24
  {
    return (((_sClock != 0)?(_sClock - _rClock):_rClock) / (3600 * _MULTIPLIER)) % 24;
  }
  else if(total == 1)                                                   //if total = 1 return total hours
  {
    return ((_sClock != 0)?(_sClock - _rClock):_rClock) / (3600 * _MULTIPLIER);
  }
}

unsigned long cTimer::getMinutes(int total)
{
  if(total == 0)                                                        //if total = 0 (false) return minutes modulo 60
  {
    return (((_sClock != 0)?(_sClock - _rClock):_rClock) / (60 * _MULTIPLIER)) % 60;
  }
  else if(total == 1)                                                   //if total = 1 return total minutes
  {
    return ((_sClock != 0)?(_sClock - _rClock):_rClock) / (60 * _MULTIPLIER);
  }
}

unsigned long cTimer::getSeconds(int total)
{
  if(total == 0)                                                        //if total = 0 (false) return secons modulo 60
  {
    return (((_sClock != 0)?(_sClock - _rClock):_rClock) / _MULTIPLIER) % 60;
  }
  else if(total == 1)                                                   //if total = 1 return total seconds
  {
    return ((_sClock != 0)?(_sClock - _rClock):_rClock) / _MULTIPLIER;
  }
  
}

unsigned long cTimer::getMSElapsed()
{
  return _rClock;
}
