/*
 * twoBinky
 * 
 * written by Ben Tenney
 * 
 * An example of how to use a timer to create a delay on one element
 * without delaying the whole program.
 * 
 */



#include <cTimer.h>



//declare two output pins for leds
#define LED1 13
#define LED2 12

//create a timer object for each led
cTimer timer1;
cTimer timer2;

void setup()
{
  //set one timer to 2 seconds and the other to 5 seconds
  timer1.setTimer(2000);
  timer2.setTimer(5000);
  
  //set led pins to output
  pinMode(LED1, OUTPUT);
  pinMode(LED2, OUTPUT);
  
  //give each pin a default state
  digitalWrite(LED1, LOW);
  digitalWrite(LED2, LOW);
  
  //start both timers
  timer1.start();
  timer2.start();
}

void loop()
{
  //when timer1 runs out of time switch state of led and restart timer
  if(timer1.updateTimer() == -1)
  {
    digitalWrite(LED1, !digitalRead(LED1));
    timer1.reset();
    timer1.start();
  }
  //when timer2 runs out of time switch state of led and restart timer
  if(timer2.updateTimer() == -1)
  {
    digitalWrite(LED2, !digitalRead(LED2));
    timer2.reset();
    timer2.start();
  }
}

